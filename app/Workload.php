<?php

namespace App;

class Workload extends MyModel{
	public function getUser() {
		return User::getbyId($this->user_id);
	}

	public function getUserName() {
		$w = $this->getUser();
		return $w->full_name;
	}
}
