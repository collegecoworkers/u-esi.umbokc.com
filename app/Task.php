<?php

namespace App;

class Task extends MyModel{

	public function getEmp() {
		return User::getbyId($this->employer_id);
	}

	public function getWor() {
		return User::getbyId($this->worker_id);
	}

	public function getWorName() {
		return $this->getWor()->full_name;
	}

	public function getEmpName() {
		return $this->getEmp()->full_name;
	}

	public function getStatus() {
		return self::getStatusOf($this->status);
	}

	public static function getStatuses(){
		return [
			'in' => 'В работе',
			'done' => 'Завершено',
			'false' => 'Отклоненно',
		];
	}

	public static function getStatusOf($r){
		$statuss = self::getStatuses();
		if(array_key_exists($r, $statuss)) 
			return $statuss[$r];
		return $statuss['in'];
	}
}
