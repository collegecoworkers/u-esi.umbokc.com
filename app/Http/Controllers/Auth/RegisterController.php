<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Workload;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
	use RegistersUsers;

	protected $redirectTo = '/';

	public function __construct()
	{
		$this->middleware('guest');
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:3|confirmed',
		]);
	}

	protected function create(array $data)
	{
		$m = new User();

		$m->name = $data['name'];
		$m->full_name = $data['full_name'];
		$m->role = 'worker';
		$m->email = $data['email'];
		$m->password = bcrypt($data['password']);

		$m->save();

		$wl = new Workload();

		$wl->user_id = $m->id;
		$wl->task_count = 1;

		$wl->save();
		return $m;
	}
}
