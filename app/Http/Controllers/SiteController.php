<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Task,
	Workload
};

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		$this->the_before();
		return view('index')->with([
			'cur_role' => $this->cur_role,
			'items' => Task::limit(7)->orderBy('id', 'desc')->get(),
		]);
	}
	public function Tasks() {
		$this->the_before();
		$user = User::curr();
		return view('task.index')->with([
			'cur_role' => $this->cur_role,
			'items' => Task::all() ,
		]);
	}

	public function Workers() {
		$this->the_before();
		$user = User::curr();
		return view('worker.index')->with([
			'cur_role' => $this->cur_role,
			'users' => User::allWorkers(),
		]);
	}

	public function Profile() {
		$user = User::curr();
		$model = Workload::getBy('user_id', $user->id);
		return view('worker.profile')->with([
			'user' => $user,
			'model' => $model,
		]);
	}
	public function Active(Request $request) {
		$user = User::curr();
		$model = Workload::getBy('user_id', $user->id);
		$model->active = $model->active == 1 ? 0 : 1;
		$model->save();
		return redirect()->to('/profile');
	}
	public function TaskCount(Request $request) {
		$user = User::curr();
		$model = Workload::getBy('user_id', $user->id);

		$model->task_count = request()->task_count;

		$model->save();
		return redirect()->to('/profile');
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
