<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	User,
	Task
};

class TaskController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function My() {
		if (User::isWorker()) {
			return view('task.my-w')->with([
				'items' => Task::getsBy('worker_id', User::curr()->id),
			]);
		} else {
			return view('task.my-e')->with([
				'items' => Task::getsBy('employer_id', User::curr()->id),
			]);
		}
	}

	public function Get($id) {
		$this->the_before();
		$model = Task::getBy('id', $id);
		$model->worker_id = User::curr()->id;
		$model->status = 'in';

		$model->save();
		return redirect()->to('/task/my');
	}
	public function View($id) {
		$this->the_before();
		$model = Task::getBy('id', $id);
		return view('task.view')->with([
			'cur_role' => $this->cur_role,
			'model' => $model,
		]);
	}


	public function Add() {
		$this->the_before();
		return view('task.add')->with([
			'cur_role' => $this->cur_role,
			'user' => User::curr(),
			'workers' => User::getFreeWorkers(),
		]);
	}
	public function Edit($id) {
		$this->the_before();
		$model = Task::getBy('id', $id);
		return view('task.edit')->with([
			'cur_role' => $this->cur_role,
			'model' => $model,
			'workers' => User::getFreeWorkers(),
			'statuss' => Task::getStatuses(),
		]);
	}
	public function Delete($id) {
		$this->the_before();
		Task::where('id', $id)->delete();
		return redirect()->to('/task/my');
	}
	public function Create(Request $request) {
		$this->the_before();
		$model = new Task();

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->employer_id = User::curr()->id;
		$model->worker_id = request()->worker_id;

		$model->save();
		return redirect()->to('/task/my');
	}
	public function Update($id, Request $request) {
		$this->the_before();
		$model = Task::getBy('id', $id);

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->status = request()->status;

		$model->save();
		return redirect()->to('/task/my');
	}

	public function the_before() {
		$this->cur_role = User::curRole();
	}
}
