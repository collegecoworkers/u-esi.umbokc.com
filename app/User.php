<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
	use Notifiable;

	protected $fillable = [ 'name', 'email', 'password', ];
	protected $hidden = [ 'password', 'remember_token'];

	public function getRole() {
		return self::getRoleOf($this->role);
	}

	public static function getFreeWorkers(){
		$wl = Workload::getsBy(['active' => 1]);
		$workers = [];

		foreach ($wl as $item) {
			$user = User::getById($item->user_id);
			$count_works = count(Task::getsBy(['status' => 'in', 'worker_id' => $user->id]));
			if($count_works < $item->task_count && $user->role == 'worker')
				$workers[$user->id] = $user->full_name;
		}

		return $workers;
	}

	public static function getRoles(){
		return [
			'worker' => 'Работник',
			'employer' => 'Работодатель',
			'admin' => 'Админ',
		];
	}

	public static function getRoleOf($r){
		$roles = self::getRoles();
		if(array_key_exists($r, $roles)) 
			return $roles[$r];
		return $roles[0];
	}

	public static function allWorkers() {
		return User::getsBy('role', 'worker');
	}

	public static function isAdmin() { return self::curRole() == 'admin'; }
	public static function isEmployer() { return self::curRole() == 'employer'; }
	public static function isWorker() { return self::curRole() == 'worker'; }
	public static function curr() { return auth()->user(); }
	public static function curRole() { return self::curr()->role; }

	public static function getById($val){ return self::getBy('id', $val); }
	public static function getBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->first(); else return self::queryBy([$col => $val])->first(); }
	public static function getsBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->get(); else return self::queryBy([$col => $val])->get(); }
	public static function queryBy($arr){ return self::where($arr); }
}
