<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index')->name('/');

Route::get('/task/all', 'SiteController@Tasks')->name('/task/all');
Route::get('/worker/all', 'SiteController@Workers')->name('/worker/all');

Route::get('/task/my', 'TaskController@My')->name('/task/my');

Route::get('/profile', 'SiteController@Profile')->name('/profile');
Route::post('/active', 'SiteController@Active')->name('/active');
Route::post('/task_count', 'SiteController@TaskCount')->name('/task_count');

Route::get('/task/add', 'TaskController@Add')->name('/task/add');
Route::get('/task/view/{id}', 'TaskController@View')->name('/task/view/{id}');
Route::get('/task/edit/{id}', 'TaskController@Edit')->name('/task/edit/{id}');
Route::get('/task/delete/{id}', 'TaskController@Delete')->name('/task/delete/{id}');
Route::post('/task/create', 'TaskController@Create')->name('/task/create');
Route::post('/task/update/{id}', 'TaskController@Update')->name('/task/update/{id}');

Route::get('/users', 'UserController@Index')->name('/users');
Route::get('/user/add', 'UserController@Add')->name('/user/add');
Route::get('/user/edit/{id}', 'UserController@Edit')->name('/user/edit/{id}');
Route::get('/user/delete/{id}', 'UserController@Delete')->name('/user/delete/{id}');
Route::post('/user/create', 'UserController@Create')->name('/user/create');
Route::post('/user/update/{id}', 'UserController@Update')->name('/user/update/{id}');
