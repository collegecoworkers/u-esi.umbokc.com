@php
use App\User;
use App\Task;
$curr = User::curr();
@endphp
@extends('layouts.app')
@section('content')
<div class="row heading-bg  bg-red">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-light">Главная</h5>
	</div>
</div>
<div class="row">
	<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
		<div class="panel panel-default card-view pa-0">
			<div class="panel-wrapper collapse in">
				<div class="panel-body pa-0">
					<div class="sm-data-box bg-green">
						<div class="row ma-0">
							<div class="col-xs-5 text-center pa-0 icon-wrap-left">
								<i class="icon-user txt-light"></i>
							</div>
							<div class="col-xs-7 text-center data-wrap-right">
								<h6 class="txt-light">Пользователей</h6>
								<span class="txt-light counter counter-anim">{{ User::count() }}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
		<div class="panel panel-default card-view pa-0">
			<div class="panel-wrapper collapse in">
				<div class="panel-body pa-0">
					<div class="sm-data-box bg-green">
						<div class="row ma-0">
							<div class="col-xs-5 text-center pa-0 icon-wrap-left">
								<i class="icon-layers txt-light"></i>
							</div>
							<div class="col-xs-7 text-center data-wrap-right">
								<h6 class="txt-light">Всего заданий</h6>
								<span class="txt-light counter counter-anim">{{ Task::count() }}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
		<div class="panel panel-default card-view pa-0">
			<div class="panel-wrapper collapse in">
				<div class="panel-body pa-0">
					<div class="sm-data-box bg-green">
						<div class="row ma-0">
							<div class="col-xs-5 text-center pa-0 icon-wrap-left">
								<i class="icon-like txt-light"></i>
							</div>
							<div class="col-xs-7 text-center data-wrap-right">
								<h6 class="txt-light">Завершено заданий</h6>
								<span class="txt-light counter counter-anim">{{ Task::where('status', 'done')->count() }}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
		<div class="panel panel-default card-view pa-0">
			<div class="panel-wrapper collapse in">
				<div class="panel-body pa-0">
					<div class="sm-data-box bg-green">
						<div class="row ma-0">
							<div class="col-xs-5 text-center pa-0 icon-wrap-left">
								<i class="icon-paper-clip txt-light"></i>
							</div>
							<div class="col-xs-7 text-center data-wrap-right">
								<h6 class="txt-light">Ваших заданий</h6>
								@if (User::isWorker())
								<span class="txt-light counter counter-anim">{{ Task::where('worker_id', $curr->id)->count() }}</span>
								@else
								<span class="txt-light counter counter-anim">{{ Task::where('employer_id', $curr->id)->count() }}</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="wrapper to-do">
			<header>
				<h2>Новинки среди заданий</h2>
			</header>
			<div class="card-block">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Название</th>
							<th>Работодатель</th>
							<th>Работник</th>
							<th>Статус</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($items as $item)
						<tr>
							<td>{{ $item->id }}</td>
							<td>{{ $item->title }}</td>
							<td>{{ $item->getEmp()->full_name }}</td>
							<td>{{ $item->getWorName() }}</td>
							<td>{{ $item->getStatus() }}</td>
							<td>
								<a href="{{ route('/task/view/{id}', ['id' => $item->id]) }}">
									<i class="fa fa-eye"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
