@extends('../layouts.app')
@section('content')
<div class="row heading-bg  bg-red">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-light">Задание</h5>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="wrapper to-do panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">{{ $model->title }}</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<b c#0>Описание: </b>
					<p>{!! $model->desc !!}</p>
				</div>
				<div class="panel-footer txt-dark font-12">
					<b>Работник: </b>
					<p p:l>{{ $model->getWorName() }}</p>
				</div>
				<div class="panel-footer txt-dark font-12">
					<b>Статус: </b>
					<p p:l>{{ $model->getStatus() }}</p>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
@endsection
