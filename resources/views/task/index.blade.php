@php
use App\User;
use App\Task;
$curr = User::curr();
@endphp
@extends('layouts.app')
@section('content')
<div class="row heading-bg  bg-red">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-light">Задания</h5>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="wrapper to-do">
			<header>
				<h2>Все задания</h2>
			</header>
			<div class="card-block">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Название</th>
							<th>Работодатель</th>
							<th>Работник</th>
							<th>Статус</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($items as $item)
						<tr>
							<td>{{ $item->id }}</td>
							<td>{{ $item->title }}</td>
							<td>{{ $item->getEmp()->full_name }}</td>
							<td>{{ $item->getWorName() }}</td>
							<td>{{ $item->getStatus() }}</td>
							<td>
								<a href="{{ route('/task/view/{id}', ['id' => $item->id]) }}">
									<i class="fa fa-eye"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
