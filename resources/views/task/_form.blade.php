{!! Form::open(['enctype'=>'multipart/form-data', 'url' => isset($model) ? '/task/update/'.$model->id : '/task/create']) !!}
	<div class="form-group">
		<label class="control-label mb-10 text-left">Название:</label>
		<input type="text" class="form-control" name="title" value="{{ isset($model) ? $model->title : ''}}" />
	</div>
	<div class="form-group">
		<label class="control-label mb-10 text-left">Описание:</label>
		<textarea class="form-control" rows="10" name="desc">{{ isset($model) ? $model->desc : ''}}</textarea>
	</div>
	<div class="form-group">
		<label class="control-label mb-10 text-left">Работник:</label>
		{!! Form::select('worker_id', $workers,  isset($model) ? $model->worker_id : '', ['class' => 'form-control']) !!}
	</div>
	@isset($model)
		<div class="form-group">
			<label class="control-label mb-10 text-left">Статус:</label>
			{!! Form::select('status', $statuss,  $model->status, ['class' => 'form-control']) !!}
		</div>
	@endisset
	<div class="form-actions">
		<button type="submit" class="btn btn-success">Сохранить</button>
	</div>
{!! Form::close() !!}
