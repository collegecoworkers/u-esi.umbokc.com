@extends('../layouts.app')
@section('content')
<div class="row heading-bg  bg-red">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-light">Мои задания</h5>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="wrapper to-do">
			<header>
				<h2>Мои задания</h2>
			</header>
			<a m:v:big href="{{ route('/task/add') }}" class="btn btn-primary">Добавить</a>
			<div class="card-block">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Название</th>
							<th>Работник</th>
							<th>Статус</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($items as $item)
						<tr>
							<td>{{ $item->id }}</td>
							<td>{{ $item->title }}</td>
							<td>{{ $item->getWorName() }}</td>
							<td>{{ $item->getStatus() }}</td>
							<td>
								<a href="{{ route('/task/view/{id}', ['id'=>$item->id]) }}">
									<i class="fa fa-eye"></i>
								</a>
								<a href="{{ route('/task/edit/{id}', ['id'=>$item->id]) }}">
									<i class="fa fa-pencil"></i>
								</a>
								<a href="{{ route('/task/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
									<i class="fa fa-trash"></i>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
