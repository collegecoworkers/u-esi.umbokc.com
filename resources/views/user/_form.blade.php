{!! Form::open(['url' => isset($model) ? '/user/update/'.$model->id : '/user/create', 'class' => 'form-horizontal']) !!}

	<div class="form-group">
		<label>Имя:</label>
    {!! Form::text('full_name', isset($model) ? $model->full_name : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Логин:</label>
    {!! Form::text('name', isset($model) ? $model->name : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Email:</label>
    {!! Form::text('email', isset($model) ? $model->email : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Роль:</label>
		{!! Form::select('role', $roles, isset($model) ? $model->role : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Пароль:</label>
    {!! Form::password('password', ['class' => 'form-control']) !!}
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-success">Сохранить</button>
	</div>
{!! Form::close() !!}
