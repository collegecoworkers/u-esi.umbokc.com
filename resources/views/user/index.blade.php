@extends('../layouts.app')
@section('content')
<div class="row heading-bg  bg-red">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-light">Пользователи</h5>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Список пользователей</h6>
				</div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap mt-40">
						<a href="{{ route('/user/add') }}" class="btn btn-primary">Добавить</a>
						<br>
						<br>
						<div class="table-responsive">
							<table class="table mb-0">
								<thead>
									<tr>
										<th>#</th>
										<th>Имя</th>
										<th>Логин</th>
										<th>Email</th>
										<th>Статус</th>
										<th>Действия</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($users as $item)
										<tr>
											<td ta:c>{{$item->id}}</td>
											<td ta:c>{{$item->full_name}}</td>
											<td ta:c>{{$item->name}}</td>
											<td ta:c>{{$item->email}}</td>
											<td ta:c>{{$item->getRole()}}</td>
											<td>
												<a href="{{ route('/user/edit/{id}', ['id'=>$item->id]) }}">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="{{ route('/user/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
													<i class="fa fa-trash"></i>
												</a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
