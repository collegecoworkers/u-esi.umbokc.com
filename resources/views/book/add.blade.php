@extends('../layouts.app')
@section('content')
<div id="content-header">
	<div id="breadcrumb">
		<a href="/" title="Go to Home" class="tip-bottom">
			<i class="icon-home"></i> Главная</a>
			<a href="{{ url()->current() }}" class="current">Добавить книгу</a>
		</div>
		<h1>Добавить новую книгу</h1>
	</div>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span12">
				<div class="widget-box">
					<div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
						<h5>Информация</h5>
					</div>
					<div class="widget-content nopadding">
						@include('book._form')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
