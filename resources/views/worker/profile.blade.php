@extends('../layouts.app')
@section('content')
<div class="row heading-bg  bg-red">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-light">Профиль</h5>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="wrapper to-do panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Текущий статус: {{ $model->active == 1 ? '' : 'Не'}} активен </h5>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					{!! Form::open(['url' => '/active']) !!}
						<button type="submit" class="btn btn-primary">Изменить статус</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		<div class="wrapper to-do panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Максимальное кол-во заданий</h5>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					{!! Form::open(['url' => '/task_count']) !!}
						<div class="form-group">
							{!! Form::text('task_count', $model->task_count, ['class' => 'form-control']) !!}
						</div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Изменить</button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
</section>
@endsection
