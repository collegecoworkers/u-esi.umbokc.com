@php
use App\Workload;
use App\Task;
@endphp
@extends('../layouts.app')
@section('content')
<div class="row heading-bg  bg-red">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h5 class="txt-light">Работники</h5>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-heading">
				<div class="pull-left">
					<h6 class="panel-title txt-dark">Список работников</h6>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="table-wrap mt-40">
						<div class="table-responsive">
							<table class="table mb-0">
								<thead>
									<tr>
										<th>#</th>
										<th>Имя</th>
										<th>Email</th>
										<th>Активен</th>
										<th>Загруженность</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($users as $item)
										@php
											$wl = Workload::getBy('user_id', $item->id);
											$count_works = count(Task::getsBy([
												'status' => 'in',
												'worker_id' => $item->id,
											]));
											$percent = ceil(floatval(($count_works / $wl->task_count * 100)));
										@endphp
										<tr>
											<td>{{ $item->id }}</td>
											<td>{{ $item->full_name }}</td>
											<td>{{ $item->email }}</td>
											<td>{{ $wl->active == 1 ? 'Да' : 'Нет' }}</td>
											<td>
												<div class="panel-body" style="padding-left: 0;margin-left: -70px;" m:t>
													<div class="progress progress-lg">
														<div class="progress-bar progress-bar-danger" style="width: {{$percent}}%;" role="progressbar">
															{{ $count_works . '/' . $wl->task_count }}
														</div>
													</div>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>	
@endsection
