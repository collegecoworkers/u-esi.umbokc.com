@extends('../layouts.auth')
@section('content')
<div class="panel panel-default card-view mb-0">
  <div class="panel-heading">
    <div class="pull-left">
      <h6 class="panel-title txt-dark">Регистрация</h6>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="panel-wrapper collapse in">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12 col-xs-12">
          <div class="form-wrap">
            <form method="post" action="{{ route('register') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label mb-10" for="full_name">Имя</label>
                <div class="input-group">
                  <input type="text" class="form-control" name="full_name" required="" id="full_name" placeholder="Имя">
                  <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                </div>
                @if ($errors->has('full_name'))<span class="help-block"><strong c#f>{{ $errors->first('full_name') }}</strong></span> @endif
              </div>
              <div class="form-group">
                <label class="control-label mb-10" for="name">Логин</label>
                <div class="input-group">
                  <input type="text" class="form-control" name="name" required="" id="name" placeholder="Логин">
                  <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                </div>
                @if ($errors->has('name'))<span class="help-block"><strong c#f>{{ $errors->first('name') }}</strong></span> @endif
              </div>
              <div class="form-group">
                <label class="control-label mb-10" for="email">Email</label>
                <div class="input-group">
                  <input type="email" class="form-control" name="email" required="" id="email" placeholder="Email">
                  <div class="input-group-addon"><i class="icon-envelope-open"></i></div>
                </div>
                @if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span> @endif
              </div>
              <div class="form-group">
                <label class="control-label mb-10" for="password">Пароль</label>
                <div class="input-group">
                  <input type="password" class="form-control" name="password" required="" id="password" placeholder="Пароль">
                  <div class="input-group-addon"><i class="icon-lock"></i></div>
                </div>
                @if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span> @endif
              </div>
              <div class="form-group">
                <label class="control-label mb-10" for="password_confirmation">Пароль еще раз</label>
                <div class="input-group">
                  <input type="password" class="form-control" name="password_confirmation" required="" id="password_confirmation" placeholder="Пароль">
                  <div class="input-group-addon"><i class="icon-lock"></i></div>
                </div>
                @if ($errors->has('password_confirmation'))<span class="help-block"><strong c#f>{{ $errors->first('password_confirmation') }}</strong></span> @endif
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-success btn-block" value="Зарегистрироваться" />
              </div>
              <div class="form-group mb-0">
                <a href="{{ route('login') }}" class="inline-block txt-danger">Войти</a>
              </div>  
            </form>
          </div>
        </div>  
      </div>
    </div>
  </div>
</div>
@endsection
