@extends('../layouts.auth')
@section('content')
<div class="panel panel-default card-view mb-0">
	<div class="panel-heading">
		<div class="pull-left">
			<h6 class="panel-title txt-dark">Вход</h6>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-wrapper collapse in">
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<div class="form-wrap">
						<form method="post" action="{{ route('login') }}">
							{{ csrf_field() }}
							<div class="form-group">
								<label class="control-label mb-10" for="exampleInputEmail_2">Email</label>
								<div class="input-group">
									<input type="email" class="form-control" name="email" required="" id="exampleInputEmail_2" placeholder="Email">
									<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
								</div>
								@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span> @endif
							</div>
							<div class="form-group">
								<label class="control-label mb-10" for="exampleInputpwd_2">Пароль</label>
								<div class="input-group">
									<input type="password" class="form-control" name="password" required="" id="exampleInputpwd_2" placeholder="Пароль">
									<div class="input-group-addon"><i class="icon-lock"></i></div>
								</div>
								@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span> @endif
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-success btn-block" value="Войти" />
							</div>
							<div class="form-group mb-0">
								<a href="{{ route('register') }}" class="inline-block txt-danger">Зарегистрироваться</a>
							</div>	
						</form>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
@endsection
