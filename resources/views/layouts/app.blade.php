<!DOCTYPE html>
<html lang="en" ea>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>{{ config('app.name', 'Laravel') }}</title>

	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3">
	<link href="/assets/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
	<link href="/assets/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
	<link href="/assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
	<link href="/assets/dist/css/style.css" rel="stylesheet" type="text/css">
	<script src="http://cdn.umbokc.com/ea/src/ea.js?v=1.2"></script>
</head>
<body>
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<div class="wrapper">
		@include('layouts.nav')

		@include('layouts.sidebar')		

		<div class="page-wrapper">
			<div class="container-fluid">
				@yield('content')
				<footer class="footer container-fluid pl-30 pr-30">
					<div class="row">
						<div class="text-center">
							<p>&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.</p>
						</div>
					</div>
				</footer>
			</div>
		</div>

		<script src="/assets/vendors/bower_components/jquery/dist/jquery.min.js"></script>
		<script src="/assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="/assets/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<script src="/assets/dist/js/jquery.slimscroll.js"></script>
		<script src="/assets/vendors/bower_components/moment/min/moment.min.js"></script>
		<script src="/assets/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
		<script src="/assets/dist/js/simpleweather-data.js"></script>
		<script src="/assets/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
		<script src="/assets/vendors/bower_components/Counter-Up/jquery.counterup.min.js"></script>
		<script src="/assets/dist/js/dropdown-bootstrap-extended.js"></script>
		<script src="/assets/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>
		<script src="/assets/vendors/bower_components/raphael/raphael.min.js"></script>
		<script src="/assets/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
		<script src="/assets/dist/js/init.js"></script>
		<script src="/assets/dist/js/dashboard-data.js"></script>
	</body>
	</html>
