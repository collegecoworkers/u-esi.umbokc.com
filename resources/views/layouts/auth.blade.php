<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>{{ config('app.name', 'Laravel') }}</title>
	
	<link href="/assets/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="/assets/dist/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<div class="wrapper pa-0">
		<div class="page-wrapper pa-0 ma-0">
			<div class="container-fluid">
				<div class="table-struct full-width full-height">
					<div class="table-cell vertical-align-middle">
						<div class="auth-form  ml-auto mr-auto no-float">
							@yield('content')
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<script src="/assets/vendors/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="/assets/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="/assets/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
	<script src="/assets/dist/js/jquery.slimscroll.js"></script>
	<script src="/assets/dist/js/dropdown-bootstrap-extended.js"></script>
	<script src="/assets/dist/js/init.js"></script>
</body>
</html>
