@php
use App\{User};
$curr = User::curr();
$r = User::curRole();
@endphp

<nav class="navbar navbar-inverse navbar-fixed-top">
	<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block mr-20 pull-left" href="javascript:void(0);"><i class="fa fa-bars"></i></a>
	<a href="/">
		<span lh=59px>{{ config('app.name', 'Laravel') }}</span>
	</a>
	<ul class="nav navbar-right top-nav pull-right">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><span>{{ $curr->getRole() }}: </span>{{ $curr->full_name }}</a>
			<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
				@if ($r == 'worker')
					<li>
						<a href="{{ route('/profile') }}"><i class="fa fa-fw fa-user"></i> Профиль</a>
					</li>
					<li class="divider"></li>
				@endif
				<li>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
					<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-fw fa-power-off"></i> Выйти</a>
				</li>
			</ul>
		</li>
	</ul>
</nav>
