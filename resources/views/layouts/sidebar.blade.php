@php
use App\{User};
$curr = User::curr();
$r = User::curRole();
$nav = [
  ['visible' => true, 'url' => route('/'), 'icon' => 'home', 'label' => 'Главная'],
  ['visible' => true, 'url' => route('/task/all'), 'icon' => 'layers', 'label' => 'Список заданий'],
  ['visible' => true, 'url' => route('/worker/all'), 'icon' => 'people', 'label' => 'Список работников'],
  ['visible' => $r != 'admin', 'url' => route('/task/my'), 'icon' => 'notebook', 'label' => 'Мои задания'],
  ['visible' => $r == 'admin', 'url' => route('/users'), 'icon' => 'people', 'label' => 'Пользователи'],
];
@endphp
<div class="fixed-sidebar-left">
	<ul class="nav navbar-nav side-nav nicescroll-bar">
		@foreach ($nav as $item)
			@if ($item['visible'])
				<li>
					<a href="{{ $item['url'] }}" class="{{ url()->current() == $item['url'] ? 'active' : ''}}">
						<i class="icon-{{$item['icon']}}"></i>
						<span>{{$item['label']}}</span>
					</a>
				</li>
			@endif
		@endforeach
	</ul>
</div>
